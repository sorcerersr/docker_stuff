# Rust Image mit Cargo Index

## Beschreibung

Beim bauen einer Rust-Anwendung mit cargo in einem temporären Container
muss bei jedem Bau der cargo index erneut heruntergeladen werden. 
Da dieser sehr groß ist, dauert das ziemlich lange. 

Daher gibt es dieses Docker-Image, welches eine Dummy-Anwendung baut und so einen initiallen cargo index besitzt. Dieses Image kann daher als
Builder Image für weitere Images genutzt werden, damit die dortigen Rust Builds schneller ausgeführt werden.

Nicht zum ausführen als Container gedacht, sondern nur als Basis-Image für weitere Images.

## Image

Jeden Monat am 2. um 14 Uhr wird das Image neu gebaut und in die Gitlab Registry gepuscht.

Das Image kann abgerufen werden über:
```
docker pull registry.gitlab.com/sorcerersr/docker_stuff/rust:latest
```

Zu beachten ist, dass als Basis das jeweils letzte *rust:alpine* Image verwendet wird, also ohne Versionstag. Daher für *"Reproducible Builds"* nur bedingt geeignet. Für meine Verwendungszwecke aber passend, da ich so ein Image mit der aktuellen Rust-Version und einem relativ aktuellen Cargo-Index vorliegen habe. Relativ aktuell, da maximal ein Monat alt und so nur Updates für ein Monat beim der Aktualisierung des Index berücksichtigt werden muss.
 


