# Rust Image mit Windows Target für Cross Compilation

Ein Rust-Image mit hinzugefügtem *x86_64-pc-windows-gnu* Target um für
Windows kompilieren zu können.

# Usage

gebaut werden kann im Container dann ganz einfach per *cargo*
```
cargo build --target x86_64-pc-windows-gnu
```

Beispiel:
```
docker run -it --rm --name rust_cross_win -v "$(pwd)":/src -w /src registry.gitlab.com/sorcerersr/docker_stuff/rust_cross_win cargo build --target x86_64-pc-windows-gnu
```

Das aktuelle Verzeichnis in dem man sich befindet wird so als Volume im Container als /src verfügbar gemacht und dort dann entsprechend
der Build-Befehl für *cargo* abgesetzt.